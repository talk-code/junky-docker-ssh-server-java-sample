
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.Vector;

public class SftSample {

    private static final String LOCAL_PATH = "downloaded" + File.separator;

    public void download() throws  Exception {

        String hostname = "127.0.0.1";
        String login = "docker";
        String password = "password";

        JSch ssh = new JSch();
        ssh.setKnownHosts("known_hosts.txt");
        Session session = ssh.getSession(login, hostname, 20001);
        session.setPassword(password);
        session.connect();

        ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");
        sftp.connect();

        //List files
        Vector<ChannelSftp.LsEntry> vector = sftp.ls("/");
        for(ChannelSftp.LsEntry item: vector){
            String filename = item.getFilename();
            if(filename.equals(".")||filename.equals("..")||filename.equals("data"))
                continue;
            System.out.println("Fetching file: "+filename);
            //Get File
            String filePath = "/" + filename;
            sftp.get(filePath, LOCAL_PATH + filename);
            //Remove file
            sftp.rm(filePath);
        }

        System.err.println(sftp.pwd());
        //sftp.disconnect();
        //session.disconnect();
    }


    public void parse() throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(PersonPojo.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        File LOCAL_PATH_FILE = new File(LOCAL_PATH);
        if(!LOCAL_PATH_FILE.exists())
            LOCAL_PATH_FILE.mkdirs();
        File[] files = LOCAL_PATH_FILE.listFiles();
        if(files!=null){
            for(File file: files){
                System.out.println("Processing file: "+file.getAbsolutePath());
                PersonPojo personPojo = (PersonPojo) jaxbUnmarshaller.unmarshal(file);
                System.out.println("Parsed: "+personPojo);
            }
        }

    }


    public static void main(String[] args) throws Exception {

        SftSample sftSample = new SftSample();
        sftSample.download();
        sftSample.parse();

    }

}
