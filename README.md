## SftpSample
This is a Java Main class that connects to the SFTP Server, downloads files and parses them.

## The SFTP Server

### Access credentials
These are credentials defined in Dockerfile
* username: docker
* password: password

### Build server
Creates the docker Image using the Dockerfile in this directory
```bash
./build-server.sh
```

### Start server
Starts the server using the Created docker Image
```bash
./start-server.sh
```

### Stop server
Stops the Running server
```bash
./stop-server.sh
```

### Resume server
Resumes the previously stopped server
```bash
./stop-server.sh
```

### Purge server
Stops and deletes the server container
```bash
./purge-server.sh
```