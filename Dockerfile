FROM amimof/sftp
COPY include/* /home/docker/
COPY keys/rsa_key /etc/ssh/host_keys/ssh_host_rsa_key
ENV SSH_USERNAME docker
ENV SSH_PASSWORD password
ENV SSH_GENERATE_HOSTKEYS  false
ENV DEBUG false
ENV LOG_LEVEL INFO